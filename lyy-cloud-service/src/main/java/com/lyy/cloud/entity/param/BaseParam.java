package com.lyy.cloud.entity.param;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * 接受参数对象
 * @author liushiwei
 */
@Data
public class BaseParam implements Serializable {

    /**
     * 每页显示数量
     */
    @TableField(exist = false)
    private int limit = 10;

    /**
     * 页码
     */
    @TableField(exist = false)
    private int page = 1;

    /**
     * 排序字段 排序字段的名称
     */
    @TableField(exist = false)
    private String sortName;

    /**
     * 排序字段的方式 asc，desc
     */
    @TableField(exist = false)
    private String sortOrder;

    /**
     * 开始时间：10位时间戳
     */
    @TableField(exist = false)
    private Long startTime;

    /**
     * 结束时间：10位时间戳
     */
    @TableField(exist = false)
    private Long endTime;

}
