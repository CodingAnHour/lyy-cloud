package com.lyy.cloud.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
* 字典分类表
* @author liushiwei
* @since 2022-12-26
*/
@Data
@TableName("t_cloud_dictionary_category")
public class DictionaryCategory implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 主键
    */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
    * 代码
    */
    private String code;
    /**
    * 分类名称
    */
    private String name;
    /**
    * 父级id
    */
    private Integer parentId;
    /**
    * 创建时间
    */
    private LocalDateTime createdDate;
    /**
    * 修改时间
    */
    private LocalDateTime updatedDate;

}