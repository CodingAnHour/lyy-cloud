package com.lyy.cloud.entity.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* 字典分类表参数表
* @author liushiwei
* @since 2022-12-26
*/
@Data
public class DictionaryCategoryParam extends BaseParam{

    private static final long serialVersionUID = 1L;
    /**
    * 代码
    */
    private String code;
    /**
    * 分类名称
    */
    private String name;
    /**
    * 父级id
    */
    private Integer parentId;

}