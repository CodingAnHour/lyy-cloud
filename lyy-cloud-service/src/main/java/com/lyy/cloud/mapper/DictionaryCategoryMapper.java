package com.lyy.cloud.mapper;

import com.lyy.cloud.entity.po.DictionaryCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典分类表 Mapper 接口
 * </p>
 *
 * @author liushiwei
 * @since 2022-12-26
 */
@Mapper
public interface DictionaryCategoryMapper extends BaseMapper<DictionaryCategory> {

}
