package com.lyy.cloud.service.impl;

import com.lyy.cloud.entity.po.DictionaryCategory;
import com.lyy.cloud.mapper.DictionaryCategoryMapper;
import com.lyy.cloud.service.DictionaryCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典分类表 服务实现类
 * </p>
 *
 * @author liushiwei
 * @since 2022-12-26
 */
@Service
public class DictionaryCategoryServiceImp extends ServiceImpl<DictionaryCategoryMapper, DictionaryCategory> implements DictionaryCategoryService {

}
