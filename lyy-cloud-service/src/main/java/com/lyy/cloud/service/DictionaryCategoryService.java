package com.lyy.cloud.service;

import com.lyy.cloud.entity.po.DictionaryCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典分类表 服务类
 * </p>
 *
 * @author liushiwei
 * @since 2022-12-26
 */
public interface DictionaryCategoryService extends IService<DictionaryCategory> {

}
