package com.lyy.cloud.service;

/**
 * 订单
 * @author liushiwei
 */
public interface OrderService {

    /**
     * 获取用户信息
     * @return
     */
    void getOrderByUserName();
}
