# lyy-cloud
lyy-cloud是SpringCloudAlibaba微服务框架为主体的一个分布式系统套件的整合，舍弃Netflix组件

## 框架组成
| 组件 | 版本 | 功能 |
|--|--|--|
| spring-cloud | 2021.0.1 | 基础组件|
| spring-cloud-alibaba | 2021.0.1.0 | 基础组件 |
| spring-boot | 2.6.3 | 基础组件|
| spring-cloud-starter-openfeign | 跟随spring-cloud版本 | 远程调用 |
| spring-cloud-starter-loadbalancer | 跟随spring-cloud版本 | 服务负载均衡 |
| Mybatis-Plus | 3.5.2 | mybatis增强 |
| Nacos | 2.1.2 | 服务发现、配置中心； 2.0.4以下版本不兼容 |
| Sentinel | 1.8.0 | 分布式流量防护|
| Seata | 2021.0.1.0 | 分布式事物，暂未集成（理论上应该尽量避免分布式事务） |
| ElasticSearch、Kibana、filebate | 7.8.0 | 日志收集管理 |

## 项目组成
| 名称 | 功能 |
|--|--|
| lyy-cloud-server-api | openfeign对外api |
| lyy-cloud-server | 内部服务：不对外暴露 |
| lyy-cloud-service | dao、service、entity、mapper的抽象 |
| lyy-cloud-web | 前端web服务：对外暴露 |
| lyy-cloud-utils | 工具类 |

## 配置说明
1. [Docker打包部署配置](./lyy-cloud-server/Dockerfile)
2. [Jenkins pipeline脚本部署配置](./lyy-cloud-server/Jenkinsfile)：git拉取代码 → maven构建 → SonarQube代码质量检查 → 
Docker制作自定义镜像 → 自定义镜像推送到Harbor中（或公有云私有镜像仓库）→ 将yml文件上传到k8s的master中 → 根据yml部署项目到k8s
3. [K8S部署脚本](./lyy-cloud-server/lyy-cloud-server.yml)示例，ingress仅是展示配置
4. [ElasticSearch、Kibana、filebate 日志收集配置](./readme/es.md)
5. Sentinel策略配置都是采用[nacos配置中心](./lyy-cloud-server/src/main/resources)管理的
    ![](./readme/sentinel.png)
6. [Mybatis-Plus代码生成](./lyy-cloud-web/src/test/java/com/lyy/cloud/web/CodeGenerator.java)
7. [sql文件](./sql/lyy-cloud.sql)
