package com.lyy.cloud.server.controller;

import com.lyy.cloud.dto.UserDTO;
import com.lyy.cloud.utils.result.Result;
import com.lyy.cloud.utils.result.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户
 * @author liushiwei
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController {


    /**
     * 查询用户信息
     * @param id 用户id
     * @return Result
     */
    @GetMapping(value = "/info")
    public Result getUserById(@RequestParam(value = "id")Integer id){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1);
        userDTO.setName("张某人");
        return ResultUtils.wrapSuccess(userDTO) ;
    }
    /**
     * 查询用户信息
     * @return Result
     */
    @GetMapping(value = "/get")
    public Result getUser(){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1);
        userDTO.setName("张某人");
        log.info("-----------------测试链接--------------------");
        int i = 1 / 0;
        return ResultUtils.wrapSuccess(userDTO) ;
    }

}
