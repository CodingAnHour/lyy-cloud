package com.lyy.cloud.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;

/**
 * 监控检查类
 * @author liushiwei
 */
@Slf4j
@RestController
public class HealthController {
    /**
     * 查询用户信息
     * @return Result
     */
    @GetMapping(value = "/health")
    public void checkUp(HttpServletResponse response){
        log.info("----------------------开始健康检查---------------------");
        response.setStatus(200);
        log.info("----------------------结束健康检查---------------------");
    }
}
