package com.lyy.cloud.utils;

/**
 * @author liushiwei
 */
public class StringVerifyUtil {
    /**
     * 验证手机号
     **/
    public static final String REGEX_MOBILE = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";


    /**
     * 验证手机号是否合法
     * @param mobile	手机号
     * @return  boolean true合法 false不合法
     * @Date 2021/2/26 11:05
     *
     **/
    public static boolean isMobile(String mobile){
        if (mobile.matches(REGEX_MOBILE)){
            return true;
        }else {
            return false;
        }
    }

}
