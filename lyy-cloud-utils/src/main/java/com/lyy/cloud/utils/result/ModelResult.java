package com.lyy.cloud.utils.result;

/**
 * 单实体结果
 */
public class ModelResult extends Result {
    private Object data;

    public ModelResult(ResultStatusEnum resultStatus){
        super(resultStatus);
    }

    public ModelResult(ResultStatusEnum resultStatus, Object data){
        this(resultStatus.getStatus(),resultStatus.getMsg(),data);
    }

    public ModelResult(int status,String msg){
        super(status, msg);
    }

    public ModelResult(int status,String msg, Object data){
        this(status, msg);
        this.data = data;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
