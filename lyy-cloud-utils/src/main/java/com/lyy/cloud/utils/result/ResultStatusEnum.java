package com.lyy.cloud.utils.result;

public enum ResultStatusEnum {

    SUCCESS(0,"操作成功"),
    FAIL(-1,"操作失败")
    ;

    private int status;
    private String msg;

    ResultStatusEnum(int status, String msg){
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
