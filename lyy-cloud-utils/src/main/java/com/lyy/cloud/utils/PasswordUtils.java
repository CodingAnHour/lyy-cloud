package com.lyy.cloud.utils;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;


/**
 * @author liushiwei
 */
public class PasswordUtils {
    public static String getMd5PasswordWithSalt(String password,String salt){
        String newPassword = new SimpleHash("MD5", password,  ByteSource.Util.bytes(salt), 1024).toHex();
        return newPassword;
    }
}
