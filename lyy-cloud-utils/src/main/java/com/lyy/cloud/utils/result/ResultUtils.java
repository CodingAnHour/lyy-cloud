package com.lyy.cloud.utils.result;

import com.lyy.cloud.utils.enumeration.ResultCode;

import java.util.Collection;

public class ResultUtils {

    public static ListResult wrapSuccess(long total, Collection rows){
        ListResult listResult = new ListResult(ResultStatusEnum.SUCCESS,total,rows);
        return listResult;
    }

    public static ListResult wrapListFail(String msg){
        return new ListResult(ResultStatusEnum.FAIL.getStatus(),msg);
    }

    public static com.lyy.cloud.utils.result.ModelResult wrapSuccess(Object data){
        return new ModelResult(ResultStatusEnum.SUCCESS,data);
    }

    public static Result wrapSuccess(){
        return new Result(ResultStatusEnum.SUCCESS.getStatus(),ResultStatusEnum.SUCCESS.getMsg());
    }

    public static Result wrapFail(){
        return new Result(ResultStatusEnum.FAIL.getStatus(),ResultStatusEnum.FAIL.getMsg());
    }

    public static Result wrapFail(String msg){
        return new Result(ResultStatusEnum.FAIL.getStatus(),msg);
    }


    public static Result wrapFail(int status,String msg){
        return new Result(status,msg);
    }

    public static Result wrapFailEnum(ResultCode code){
        return new Result(code.getCode(),code.getMessage());
    }


}
