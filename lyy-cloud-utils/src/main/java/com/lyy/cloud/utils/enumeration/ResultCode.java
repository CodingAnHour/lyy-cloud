package com.lyy.cloud.utils.enumeration;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 返回的状态码
 * @author liushiwei
 */

public enum ResultCode {
    /**
     * 成功状态码
     */
    CODE_SUCCESS(200,"操作成功"),

    /**
     * 未登陆
     */
    CODE_NOT_LOGIN(4001,"未登陆"),
    /**
     * 未授权域名
     */
    CODE_NOT_AUTHORIZED_DOMAIN(4002,"未授权域名"),
    /**
     * app 不存在
     */
    CODE_NOT_APP(4003,"app 不存在"),
    /**
     * 用户名或密码错误
     */
    CODE_USERNAME_ERROR(4004,"账号或密码错误"),
    /**
     * 短信登录用户名或验证码错误
     */
    CODE_USERNAME_SMS_ERROR(4004,"短信登录用户名或验证码错误"),
    /**
     * 用户锁定
     */
    CODE_USERNAME_LOCK(4005,"用户锁定"),
    /**
     * 注册用户已存在
     */
    CODE_USER_EXIST(4006,"注册用户已存在"),
    /**
     * 用户绑定已存在
     */
    CODE_BIND_EXIST(4007,"用户绑定已存在"),
    /**
     * 用户验证码，当日已发送上限
     */
    CODE_UPPER_LIMIT(4008,"用户验证码发送上限"),
    /**
     * 用户手机号错误
     */
    CODE_MOBILE_ERROR(4009,"用户手机号错误"),
    /**
     * 验证码错误
     */
    CODE_VERIFY_CODE_ERROR(4010,"验证码错误"),
    /**
     * 无法获取对应平台信息
     */
    CODE_PLATFORM_LOGIN_ERROR(4011,"无法获取对应平台信息"),

    /**
     * 错误状态码
     */
    CODE_ERROR(5001,"错误状态码"),
    /**
     * 警告
     */
    CODE_WARNING(5002,"警告"),
    /**
     * 无权限
     */
    CODE_NOT_JUR(5003,"无权限"),

    /**
     * 请求 参数错误
     */
    REQUEST_PARAM_ERROR(6001,"参数错误"),
    /**
     * 请求 操作频繁，请稍后重试
     */
    REQUEST_FREQUENTLY(6002,"操作频繁，请稍后重试"),
    /**
     * 请求 请不要重复操作
     */
    REQUEST_REPEAT(6003,"已操作过，请不要重复操作"),
    /**
     * 请求 内容不存在
     */
    REQUEST_NOT_EXIST(6004,"内容不存在"),
    /**
     * sentinel 接口限流
     */
    SENTINEL_FLOW(7001,"网络拥堵，请稍后重试"),
    /**
     * sentinel 服务降级
     */
    SENTINEL_DEGRADE(7002,"服务发生故障，请稍后重试"),
    /**
     * sentinel 热点参数限流
     */
    SENTINEL_PARAM_FLOW(7003,"网络拥堵，请稍后重试"),
    /**
     * sentinel 触发系统保护规则
     */
    SENTINEL_SYSTEM_BLOCK(7004,"网络拥堵，请稍后重试"),
    /**
     * sentinel 授权规则不通过
     */
    SENTINEL_AUTHORITY(7005,"权限不足，请稍后重试"),
    ;





    /**
     * code
     */
    private Integer code;
    /**
     * 消息
     */
    private String message;

    ResultCode(Integer code,String message) {

        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }

    /**
     * 枚举列表
     * @return
     */
    public static JSONArray getEnumCodeList(){
        ResultCode[] values = ResultCode.values();
        JSONArray array = new JSONArray();
        for (ResultCode value : values) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code",value.code);
            jsonObject.put("message",value.message);
            array.add(jsonObject);
        }
        return array;
    }
}
