# es模版
```json
PUT _template/filebeat_java_log_template
{
  "order": 1,
  "index_patterns": [
      "filebeat_java_*"
  ],
  "settings": {
    "index":{
      "number_of_shards": "1",
      "auto_expand_replicas":"0-1",
      "lifecycle":{
        "name":"java_log_service",
        "rollover_alias":"java_log_service"
      },
      "mapping":{
        "nested_fields":{
          "limit":100
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "app_name":{
        "type":"keyword"
      },
      "log_level":{
        "type":"keyword"
      },
      "log_thread":{
        "type":"keyword"
      },
      "message":{
        "type": "text",
        "analyzer": "ik_max_word",
        "search_analyzer": "ik_smart",
        "norms": false
      },
      "@timestamp":{
        "type": "date"
      }
    }
  }
}
```


# es ingest
```json
PUT /_ingest/pipeline/filebeat_java_log_pipeline
{
  "description": "filebeat_java_log_pipeline",
  "processors": [
    {
      "grok": {
        "field": "message",
        "patterns": ["\\|-%{TIMESTAMP_ISO8601:timestamp} %{USERNAME:app_name} %{USERNAME:log_thread} %{LOGLEVEL:log_level}"],
        "pattern_definitions": {
          "ALL_CODE": "(\n)*"
        }
      }
    },
    {
      "date": {
        "field": "timestamp",
        "formats": ["yyyy-MM-dd HH:mm:ss.SSS"],
        "timezone": "Asia/Shanghai",
        "target_field": "@timestamp"
      }
    }
  ]
}
```

# filebeat.yml 文件
docker部署filebeat，需要挂载目录对应容器目录 /var/log/messages/*.log
```yaml
filebeat.config:
  modules:
    path: ${path.config}/modules.d/*.yml
    reload.enabled: false


#---------定义应用的input类型、以及存放的具体路径
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /var/log/messages/*.log
  tags: ['java-app']
  multiline:
    # 解决多行日志问题
    pattern: '^\|\-'
    negate: true
    match: 'after'
  fields:
    # 索引模版
    index: 'filebeat_java_log_template'

# 定义模板的相关信息
setup:
  ilm:
    # 自定义ES的索引需要把ilm设置为false
    enabled: false
  template:
    enabled: true
    # 模版名称
    name: "filebeat_java_log_template"
    # 模版匹配值
    pattern: "filebeat_java_*"
    settings:
      index.number_of_shards: 1
      index.number_of_replicas: 0


#-------------------------- Elasticsearch output ------------------------------

output.elasticsearch:
  hosts: ['192.168.0.50:9200']
  username: 'elastic'
  password: 'KeUf0N2AMlldK6ELLmGH'
  indices:
    # 索引名称
    - index: "filebeat_java_%{+yyyy.MM.dd}"
      when.contains:
        tags: 'java-app'
  pipeline: 'filebeat_java_log_pipeline'


processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~
  # 过滤默认生成的字段
  - drop_fields:
      fields: ['log','host','input','agent','ecs','fields']

#============================== Kibana =====================================

# setup.kibana:
#   host: "192.168.1.227:5601"
#   username: 'elastic'
#   password: 'KeUf0N2AMlldK6ELLmGH'
# setup.dashboards:
#   enabled: true
```

