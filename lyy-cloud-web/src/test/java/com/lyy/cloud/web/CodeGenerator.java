package com.lyy.cloud.web;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import java.util.HashMap;
import java.util.Map;

/**
 * 代码生成
 */
public class CodeGenerator {
    /**
     * 项目路径
     */
    private static final String PARENT_DIR = System.getProperty("user.dir");
    /**
     * xml路径
     */
    private static final String XML_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/mapper/xml";
    /**
     * entity路径
     */
    private static final String ENTITY_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/entity/po";
    /**
     * param路径
     */
    private static final String ENTITY_PARAM_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/entity/param";
    /**
     * mapper路径
     */
    private static final String MAPPER_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/mapper";
    /**
     * service路径
     */
    private static final String SERVICE_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/service";
    /**
     * serviceImpl路径
     */
    private static final String SERVICE_IMPL_PATH = PARENT_DIR + "/lyy-cloud-service/src/main/java/com/lyy/cloud/service/impl";
    /**
     * controller路径
     */
    private static final String CONTROLLER_PATH = PARENT_DIR + "/lyy-cloud-web/src/main/java/com/lyy/cloud/web/controller";
    /**
     * 数据库url
     */
    private static final String DB_URL = "jdbc:mysql://127.0.0.1:33006/lyy-cloud?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT%2B8";
    /**
     * 数据库用户名
     */
    private static final String USERNAME = "root";
    /**
     * 数据库密码
     */
    private static final String PASSWORD = "root";

    public static void main(String[] args) {
        // 要构建代码的表名
        String[] tableNames = {"t_cloud_dictionary_category"};

        FastAutoGenerator.create(DB_URL, USERNAME, PASSWORD)
                // 全局配置
                .globalConfig(builder -> builder
                        .author("liushiwei")
                )
                // 包配置
                .packageConfig(builder -> builder
                        .parent("")
                        .xml("xml")
                        .entity("com.lyy.cloud.entity.po")
                        .mapper("com.lyy.cloud.mapper")
                        .service("com.lyy.cloud.service")
                        .serviceImpl("com.lyy.cloud.service.impl")
                        .controller("com.lyy.cloud.web.controller")
                        .pathInfo(getPathInfo())
                )
                // 策略配置
                .strategyConfig(builder -> builder
                        // 生成的表名
                        .addInclude(tableNames)
                        .enableCapitalMode()
                        .addTablePrefix("t_cloud_")
                        // entity
                        .entityBuilder()
                        // 文件覆盖
                        .fileOverride()
                        .enableChainModel()
                        .fileOverride()
                        .enableLombok()
                        .enableRemoveIsPrefix()
                        .logicDeleteColumnName("is_delete")
                        .idType(IdType.ASSIGN_ID)
                        .addTableFills(new Column("create_time", FieldFill.INSERT))
                        .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))
                        // controller
                        .controllerBuilder()
                        .fileOverride()
                        .enableRestStyle()
                        .formatFileName("%sController")
                        // service
                        .serviceBuilder()
                        .fileOverride()
                        .superServiceClass(IService.class)
                        .formatServiceFileName("%sService")
                        .formatServiceImplFileName("%sServiceImp")
                        // mapper
                        .mapperBuilder()
                        .fileOverride()
                        .enableBaseResultMap()
                        .enableBaseColumnList()
                        .superClass(BaseMapper.class)
                        .formatMapperFileName("%sMapper")
                        .formatXmlFileName("%sMapper")
                        .enableMapperAnnotation()
                )
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new FreemarkerTemplateEngine())
                //设置自定义模板路径
                .templateConfig(builder -> {
                    builder.controller("/templates/controller.java");
                    builder.mapper("templates/mapper.java");
                    builder.entity("templates/entity.java");
                })
                .execute();
    }

    /**
     * 获取路径信息map
     *
     * @return {@link Map< OutputFile, String> }
     * @author MK
     * @date 2022/4/21 21:21
     */
    private static Map<OutputFile, String> getPathInfo() {
        Map<OutputFile, String> pathInfo = new HashMap<>(5);
        pathInfo.put(OutputFile.entity, ENTITY_PATH);
        pathInfo.put(OutputFile.mapper, MAPPER_PATH);
        pathInfo.put(OutputFile.service, SERVICE_PATH);
        pathInfo.put(OutputFile.serviceImpl, SERVICE_IMPL_PATH);
        pathInfo.put(OutputFile.controller, CONTROLLER_PATH);
        pathInfo.put(OutputFile.xml, XML_PATH);
        pathInfo.put(OutputFile.other, ENTITY_PARAM_PATH);
        return pathInfo;
    }
}