package com.lyy.cloud.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WebApplicationTests {

    @Test
    void contextLoads() {
    }

    public static void main(String[] args) {
        String PARENT_DIR = System.getProperty("user.dir");
        System.out.println(PARENT_DIR);
    }
}
