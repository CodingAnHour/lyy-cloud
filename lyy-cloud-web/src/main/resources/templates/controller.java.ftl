package ${package.Controller};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyy.cloud.entity.po.${entity};
import com.lyy.cloud.entity.param.${entity}Param;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ${package.Service}.${table.serviceName};
import com.lyy.cloud.utils.result.Result;
import com.lyy.cloud.utils.result.ResultUtils;
import java.time.LocalDateTime;

/**
* <p>
* ${table.comment!} Controller
* </p>
*
* @author ${author}
* @since ${date}
*/
@Slf4j
@RestController
@RequestMapping("/${table.entityPath}")
public class ${table.controllerName} extends BaseController{

    @Autowired
    private ${table.serviceName} ${table.entityPath}Service;

    /**
    * 分页列表
    */
    @RequestMapping(value="getList", method= RequestMethod.POST)
    public Result getList(@RequestBody ${entity}Param param) {
        Page<${entity}> page = new Page<${entity}>(param.getPage(),param.getLimit());

        //构建条件
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>();
<#list table.fields as field>
        // ${field.comment}
    <#if field.propertyType == "String">
        if(StringUtils.isNotBlank(param.get${field.capitalName}())){
            wrapper.eq("${field.annotationColumnName}",param.get${field.capitalName}());
        }
    <#else>
        if(null != param.get${field.capitalName}()){
            wrapper.eq("${field.annotationColumnName}",param.get${field.capitalName}());
        }
    </#if>
</#list>
        wrapper.orderByDesc("created_date");

        page = ${table.entityPath}Service.page(page,wrapper);
        return ResultUtils.wrapSuccess(page.getTotal(),page.getRecords());
    }

    /**
    * 新增数据
    */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody ${entity} param){

        param.setCreatedDate(LocalDateTime.now());
        boolean bool = ${table.entityPath}Service.save(param);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 更新数据
    */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result update(@RequestBody ${entity} param){

        boolean bool = ${table.entityPath}Service.updateById(param);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 删除数据
    */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable("id") Integer id){

        boolean bool = ${table.entityPath}Service.removeById(id);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 根据id查询数据
    */
    @RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
    public Object getById(@PathVariable("id") Integer id){

        ${entity} ${entity?uncap_first}= ${table.entityPath}Service.getById(id);
        return ResultUtils.wrapSuccess(${entity?uncap_first});
    }



}