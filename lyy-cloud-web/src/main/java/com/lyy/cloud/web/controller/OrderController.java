package com.lyy.cloud.web.controller;
import com.lyy.cloud.dto.UserDTO;
import com.lyy.cloud.feign.UserFeign;
import com.lyy.cloud.utils.result.Result;
import com.lyy.cloud.utils.result.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@Slf4j
@RestController
@RequestMapping("order")
public class OrderController{
    @Autowired
    private UserFeign userFeign;


    /**
     * 查询用户信息
     * @param id 用户id
     * @return Result
     */
    @GetMapping(value = "/info")
    public Result getUserById(@RequestParam(value = "id")Integer id){
        Result<UserDTO> userResult = userFeign.getUserById(1);
        UserDTO data = userResult.getData();
        return ResultUtils.wrapSuccess(data) ;
    }
    /**
     * 查询用户信息
     * @return Result
     */
    @GetMapping(value = "/list")
    public Result getList(){
        return ResultUtils.wrapSuccess(new ArrayList<>()) ;
    }
}
