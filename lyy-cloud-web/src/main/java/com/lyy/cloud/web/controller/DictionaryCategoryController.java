package com.lyy.cloud.web.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyy.cloud.entity.po.DictionaryCategory;
import com.lyy.cloud.entity.param.DictionaryCategoryParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.lyy.cloud.service.DictionaryCategoryService;
import com.lyy.cloud.utils.result.Result;
import com.lyy.cloud.utils.result.ResultUtils;
import java.time.LocalDateTime;

/**
* <p>
* 字典分类表 Controller
* </p>
*
* @author liushiwei
* @since 2022-12-26
*/
@Slf4j
@RestController
@RequestMapping("/dictionaryCategory")
public class DictionaryCategoryController extends BaseController{

    @Autowired
    private DictionaryCategoryService dictionaryCategoryService;

    /**
    * 分页列表
    */
    @RequestMapping(value="getList", method= RequestMethod.POST)
    public Result getList(@RequestBody DictionaryCategoryParam param) {
        Page<DictionaryCategory> page = new Page<DictionaryCategory>(param.getPage(),param.getLimit());

        //构建条件
        QueryWrapper<DictionaryCategory> wrapper = new QueryWrapper<>();
        // 代码
        if(StringUtils.isNotBlank(param.getCode())){
            wrapper.eq("code",param.getCode());
        }
        // 分类名称
        if(StringUtils.isNotBlank(param.getName())){
            wrapper.eq("name",param.getName());
        }
        // 父级id
        if(null != param.getParentId()){
            wrapper.eq("parent_id",param.getParentId());
        }
        wrapper.orderByDesc("created_date");

        page = dictionaryCategoryService.page(page,wrapper);
        return ResultUtils.wrapSuccess(page.getTotal(),page.getRecords());
    }

    /**
    * 新增数据
    */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody DictionaryCategory param){

        param.setCreatedDate(LocalDateTime.now());
        boolean bool = dictionaryCategoryService.save(param);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 更新数据
    */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result update(@RequestBody DictionaryCategory param){

        boolean bool = dictionaryCategoryService.updateById(param);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 删除数据
    */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable("id") Integer id){

        boolean bool = dictionaryCategoryService.removeById(id);

        if (bool){
            return ResultUtils.wrapSuccess();
        }else{
            return ResultUtils.wrapFail();
        }
    }

    /**
    * 根据id查询数据
    */
    @RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
    public Object getById(@PathVariable("id") Integer id){

        DictionaryCategory dictionaryCategory= dictionaryCategoryService.getById(id);
        return ResultUtils.wrapSuccess(dictionaryCategory);
    }



}