package com.lyy.cloud;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * web启动类
 * @author liushiwei
 */
@SpringBootApplication
@EnableFeignClients(basePackages = "com.lyy.cloud.feign")
public class WebApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(WebApplication.class).banner(new WebBanner()).run(args);
    }

}
