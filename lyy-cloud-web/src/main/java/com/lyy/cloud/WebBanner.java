package com.lyy.cloud;

import com.lyy.cloud.utils.banner.CustomBanner;

public class WebBanner extends CustomBanner {

    @Override
    protected String getTitle() {
        return "lyy-cloud-web";
    }
}
