/*
 Navicat Premium Data Transfer

 Source Server         : localhost-master
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 127.0.0.1:33006
 Source Schema         : lyy-cloud

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 27/12/2022 20:46:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_cloud_dictionary_category
-- ----------------------------
DROP TABLE IF EXISTS `t_cloud_dictionary_category`;
CREATE TABLE `t_cloud_dictionary_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(100) NOT NULL COMMENT '代码',
  `name` varchar(100) NOT NULL COMMENT '分类名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `created_date` datetime NOT NULL COMMENT '创建时间',
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `CODE_UNIQUE` (`code`) COMMENT 'code唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典分类表';

-- ----------------------------
-- Records of t_cloud_dictionary_category
-- ----------------------------
BEGIN;
INSERT INTO `t_cloud_dictionary_category` VALUES (1, 'aaaa', '名称1', 0, '2021-09-08 21:30:20', '2022-12-27 12:45:14');
INSERT INTO `t_cloud_dictionary_category` VALUES (2, 'bbbb', '名称2', 0, '2021-09-11 10:37:41', '2022-12-27 12:45:16');
INSERT INTO `t_cloud_dictionary_category` VALUES (3, 'cccc', '名称3', 0, '2021-09-25 19:56:45', '2022-12-27 12:45:20');
INSERT INTO `t_cloud_dictionary_category` VALUES (4, 'dddd', '名称4', 0, '2022-03-22 22:16:07', '2022-12-27 12:45:22');
INSERT INTO `t_cloud_dictionary_category` VALUES (5, 'eeee', '名称5', 0, '2022-06-17 00:05:28', '2022-12-27 12:45:25');
INSERT INTO `t_cloud_dictionary_category` VALUES (6, 'fffff', '名称6', 0, '2022-06-19 11:25:27', '2022-12-27 12:45:29');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
