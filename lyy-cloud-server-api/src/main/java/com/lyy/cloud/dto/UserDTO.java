package com.lyy.cloud.dto;

import lombok.Data;

/**
 * 用户信息
 * @author liushiwei
 */
@Data
public class UserDTO {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 名字
     */
    private String name;
}
