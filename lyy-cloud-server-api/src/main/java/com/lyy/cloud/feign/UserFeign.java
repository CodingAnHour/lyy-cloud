package com.lyy.cloud.feign;

import com.lyy.cloud.dto.UserDTO;
import com.lyy.cloud.utils.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 用户服务
 * value：需要与注册中心注册的名称一致
 * path：URL路径
 * @author liushiwei
 */
@FeignClient(value = "lyy-cloud-server", path = "/user")
public interface UserFeign {

    /**
     * 查询用户信息
     * @param id 用户id
     * @return Result
     */
    @GetMapping(value = "/info")
    Result<UserDTO> getUserById(@RequestParam(value = "id")Integer id);
}
